import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import BookShelf from './BookShelf';
import * as BooksAPI from '../BooksAPI';

class BookShelfPage extends Component {

    render() {        
        return (
            <div className="list-books">
                <div className="list-books-title">
                    <h1>MyReads</h1>
                </div>
                <div className="list-books-content">
                    <div>                
                       <BookShelf onNewShelfSelected={this.props.onNewShelfSelected} title="Currently Reading" books={this.props.books.filter(book => book.shelf === 'currentlyReading')} />
                       <BookShelf onNewShelfSelected={this.props.onNewShelfSelected} title="Want to Read" books={this.props.books.filter(book => book.shelf === 'wantToRead')}/>
                       <BookShelf onNewShelfSelected={this.props.onNewShelfSelected} title="Read" books={this.props.books.filter(book => book.shelf === 'read')}/>
                    </div>
                </div>
                <div className="open-search">
                    <Link to="/search">Search books</Link>
                </div>
            </div>            
        );
    }
}

export default BookShelfPage;